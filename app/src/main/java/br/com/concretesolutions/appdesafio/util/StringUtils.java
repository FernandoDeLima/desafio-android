package br.com.concretesolutions.appdesafio.util;

public class StringUtils {

    public static final String EMPTY = "";

    public static boolean isBlank(String value){
        return value == null || value.isEmpty();
    }

    public static boolean isNotBlank(String value){
        return !isBlank(value);
    }

}
