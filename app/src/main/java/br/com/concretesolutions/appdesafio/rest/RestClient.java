package br.com.concretesolutions.appdesafio.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.res.StringRes;

import java.text.DateFormat;

import br.com.concretesolutions.appdesafio.BuildConfig;
import br.com.concretesolutions.appdesafio.R;
import br.com.concretesolutions.appdesafio.rest.service.DribbbleApiService;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

@EBean(scope = EBean.Scope.Singleton)
public class RestClient {

    @StringRes(R.string.base_url)
    protected String baseURL;

    private DribbbleApiService dribbbleApiService;

    @AfterInject
    protected void configApi(){
        Gson gson = new GsonBuilder().setDateFormat(DateFormat.DAY_OF_WEEK_FIELD)
                .setPrettyPrinting().create();
        RestAdapter parseRestAdapter;
        if (BuildConfig.DEBUG) {
            parseRestAdapter = new RestAdapter.Builder().
                    setLogLevel(RestAdapter.LogLevel.FULL).
                    setEndpoint(baseURL).
                    setConverter(new GsonConverter(gson)).
                    build();
        } else {
            parseRestAdapter = new RestAdapter.Builder().setEndpoint(baseURL).setConverter
                    (new GsonConverter(gson)).build();
        }

        dribbbleApiService = parseRestAdapter.create(DribbbleApiService.class);
    }

    public DribbbleApiService getDribbbleApiService() {
        return dribbbleApiService;
    }
}
