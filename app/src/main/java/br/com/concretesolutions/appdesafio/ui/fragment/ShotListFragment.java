package br.com.concretesolutions.appdesafio.ui.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.annotation.UiThread;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import br.com.concretesolutions.appdesafio.R;
import br.com.concretesolutions.appdesafio.model.Shot;
import br.com.concretesolutions.appdesafio.model.ShotList;
import br.com.concretesolutions.appdesafio.rest.RestClient;
import br.com.concretesolutions.appdesafio.ui.adapter.ShotRecyclerViewAdapter;
import br.com.concretesolutions.appdesafio.ui.decoration.VerticalSpaceItemDecoration;
import br.com.concretesolutions.appdesafio.ui.listener.OnScrollChangeListener;
import br.com.concretesolutions.appdesafio.ui.listener.OnScrollResetListener;
import br.com.concretesolutions.appdesafio.ui.listener.OnShotSelectedListener;
import br.com.concretesolutions.appdesafio.ui.listener.OnTitleChangeListener;
import br.com.concretesolutions.appdesafio.util.DialogUtils;
import br.com.concretesolutions.appdesafio.util.LogUtils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

@EFragment(R.layout.fragment_shot_list)
public class ShotListFragment extends Fragment {

    private static final String TAG = ShotListFragment.class.getSimpleName();

    private OnShotSelectedListener mOnShotSelectedListener;
    private OnTitleChangeListener mOnTitleChangeListener;
    private OnScrollResetListener mOnScrollResetListener;

    private ShotList mShotList;

    private ProgressDialog progressDialog;

    @Bean
    protected RestClient restClient;

    @ViewById(R.id.rv_shots)
    protected RecyclerView mRecyclerViewShots;

    @ViewById(R.id.srl_shots)
    protected SwipeRefreshLayout mSwipeLayoutShots;

    @AfterViews
    protected void setTitle() {
        mOnTitleChangeListener.onTitleChange(getString(R.string.toolbar_title_shots));
    }

    @AfterViews
    protected void showProgressDialog() {
        progressDialog = ProgressDialog.show(getActivity(), getString(R.string.shots_loading_title),
                getString(R.string.shots_loading_shots_message), false, false);
    }

    @AfterViews
    protected void setupShots() {
        mShotList = new ShotList();
        getShotsInService(mShotList.getCurrentPage());

        final float padding = getResources().getDimension(R.dimen.padding_medium);

        final ShotRecyclerViewAdapter shotsAdapter = new ShotRecyclerViewAdapter(mShotList.getShots());
        shotsAdapter.setOnClickListener(new ShotRecyclerViewAdapter.OnClickListener() {
            @Override
            public void onClick(Shot shot) {
                mOnShotSelectedListener.onShotSelected(shot);
            }
        });

        final OnScrollChangeListener onScrollChangeListener = new OnScrollChangeListener(new OnScrollChangeListener.OnLoadMoreListener() {
            @Override
            public void load() {
                if (mShotList.hasNextPage()) {
                    getShotsInService(mShotList.getNextPage());
                }
            }
        });

        mOnScrollResetListener = onScrollChangeListener;
        mRecyclerViewShots.setLayoutManager(new LinearLayoutManager(mRecyclerViewShots.getContext()));
        mRecyclerViewShots.addItemDecoration(new VerticalSpaceItemDecoration(padding));
        mRecyclerViewShots.setAdapter(shotsAdapter);
        mRecyclerViewShots.addOnScrollListener(onScrollChangeListener);
    }

    @AfterViews
    public void configureSwipeLayout() {
        mSwipeLayoutShots.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getShotsInService(ShotList.FIRST_PAGE);
            }
        });
        mSwipeLayoutShots.setSize(SwipeRefreshLayout.LARGE);
    }

    @Background
    protected void getShotsInService(final String page) {
        LogUtils.debug(TAG, "============= STARTING REQUEST TO GET SHOTS IN PAGE [" + page + "] =============");
        restClient.getDribbbleApiService().getShots(page, new Callback<ShotList>() {
            @Override
            public void success(ShotList shotList, Response response) {
                LogUtils.debug(TAG, "============= REQUEST SUCCESSFULLY COMPLETED =============");
                updateShots(page, shotList);
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgressDialog();

                final RetrofitError.Kind kind = error.getKind();
                int messageError = R.string.shots_alert_message_generic_error;
                if (RetrofitError.Kind.NETWORK.equals(kind)) {
                    messageError = R.string.shots_alert_message_connection_error;
                }

                final AlertDialog dialog = DialogUtils.createDialog(mRecyclerViewShots.getContext(), R.string.shots_alert_title_warn,
                        messageError, R.string.shots_alert_button_try_again,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                showProgressDialog();
                                getShotsInService(mShotList.getNextPage());
                            }
                        }, R.string.shots_alert_button_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                dismissProgressDialog();
                            }
                        });
                dialog.show();

                LogUtils.debug(TAG, "============= REQUEST ERROR [" + kind.name() + "] =============");
                Response response = error.getResponse();
                if (response != null) {
                    LogUtils.debug(TAG, "============= " + response.getReason() + " =============");
                }
            }
        });
        LogUtils.debug(TAG, "============= FINISHING REQUEST TO GET SHOTS IN PAGE [" + page + "] =============");
    }

    @UiThread
    protected void updateShots(final String page, ShotList shotList) {
        if (ShotList.FIRST_PAGE.equals(page)) {
            mShotList.clearInformation();
            mOnScrollResetListener.reset();
            mRecyclerViewShots.getAdapter().notifyItemRemoved(0);
            mRecyclerViewShots.getAdapter().notifyDataSetChanged();
        }
        mShotList.updateInformation(shotList.getCurrentPage(), shotList.getTotalOfPages(), shotList.getShots());
        mRecyclerViewShots.getAdapter().notifyItemInserted(mShotList.getLastIndexBeforeUpdate());

        dismissProgressDialog();
    }

    private void dismissProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        if (mSwipeLayoutShots.isRefreshing()) {
            mSwipeLayoutShots.setRefreshing(false);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mOnShotSelectedListener = (OnShotSelectedListener) activity;
            mOnTitleChangeListener = (OnTitleChangeListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnShotSelectedListener " +
                    "and OnTitleChangeListener.");
        }
    }

}
