package br.com.concretesolutions.appdesafio.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.concretesolutions.appdesafio.util.StringUtils;

public class ShotList implements Serializable {

    public static final String FIRST_PAGE = "1";

    @SerializedName("page")
    private String currentPage;
    @SerializedName("per_page")
    private Integer perPage;
    @SerializedName("pages")
    private Integer totalOfPages;
    private Integer total;
    private List<Shot> shots;

    @Expose(deserialize = false, serialize = false)
    private Integer lastIndexBeforeUpdate;

    public ShotList(){
        this.shots = new ArrayList<>();
    }

    public String getCurrentPage() {
        if(StringUtils.isBlank(currentPage)){
            return FIRST_PAGE;
        }
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getPerPage() {
        return perPage;
    }

    public void setPerPage(Integer perPage) {
        this.perPage = perPage;
    }

    public Integer getTotalOfPages() {
        return totalOfPages;
    }

    public void setTotalOfPages(Integer totalOfPages) {
        this.totalOfPages = totalOfPages;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<Shot> getShots() {
        return shots;
    }

    public void setShots(List<Shot> shots) {
        this.shots = shots;
    }

    public Integer getLastIndexBeforeUpdate() {
        return lastIndexBeforeUpdate;
    }

    public boolean hasNextPage(){
        int currentPage = Integer.valueOf(getCurrentPage());
        return currentPage < totalOfPages;
    }

    public String getNextPage() {
        int currentPage = Integer.valueOf(getCurrentPage());
        return String.valueOf(++currentPage);
    }

    public void updateInformation(String currentPage, Integer totalOfPages, List<Shot> newShots){
        this.currentPage = currentPage;
        this.totalOfPages = totalOfPages;
        this.lastIndexBeforeUpdate = this.getShots().size();
        this.shots.addAll(newShots);
    }

    public void clearInformation(){
        this.shots.clear();
    }
}
