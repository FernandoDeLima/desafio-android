package br.com.concretesolutions.appdesafio.util;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

public class DialogUtils {

    public static AlertDialog createDialog(Context context, int title, int message,
                                           int positiveButtonText,
                                           DialogInterface.OnClickListener positiveListener,
                                           int negativeButtonText,
                                           DialogInterface.OnClickListener negativeListener) {

        AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(positiveButtonText, positiveListener)
                .setNegativeButton(negativeButtonText, negativeListener)
                .setCancelable(true)
                .create();

        return dialog;

    }

}
