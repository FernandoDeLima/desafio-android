package br.com.concretesolutions.appdesafio.rest.service;

import br.com.concretesolutions.appdesafio.model.ShotList;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

public interface DribbbleApiService {

    @GET("/shots/popular")
    void getShots(@Query(value = "page") String page, Callback<ShotList> shotListCallback);

}
