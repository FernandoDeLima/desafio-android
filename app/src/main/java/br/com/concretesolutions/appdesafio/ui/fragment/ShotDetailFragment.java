package br.com.concretesolutions.appdesafio.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import br.com.concretesolutions.appdesafio.R;
import br.com.concretesolutions.appdesafio.model.Player;
import br.com.concretesolutions.appdesafio.model.Shot;
import br.com.concretesolutions.appdesafio.ui.listener.OnTitleChangeListener;

@EFragment(R.layout.fragment_shot_detail)
public class ShotDetailFragment extends Fragment {

    public static final String EXTRA_SHOT_SELECTED = "EXTRA_SHOT_SELECTED";

    private OnTitleChangeListener mOnTitleChangeListener;

    private Shot mShot;

    @ViewById(R.id.img_shot)
    protected ImageView mImageShot;

    @ViewById(R.id.txv_title)
    protected TextView mTitle;

    @ViewById(R.id.txv_number_of_views)
    protected TextView mNumberOfViews;

    @ViewById(R.id.img_avatar)
    protected ImageView mAvatar;

    @ViewById(R.id.txv_name)
    protected TextView mName;

    @ViewById(R.id.txv_description)
    protected TextView mDescription;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(Boolean.TRUE);
    }

    @AfterViews
    protected void setupScreen() {

        this.mShot = (Shot) getArguments().getSerializable(EXTRA_SHOT_SELECTED);

        mOnTitleChangeListener.onTitleChange(mShot.getTitle());

        final Context context = getActivity().getBaseContext();
        final Player player = mShot.getPlayer();

        Glide.with(context)
                .load(player.getAvatarURL())
                .fitCenter()
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(mAvatar);

        Glide.with(context)
                .load(mShot.getImageURL())
                .centerCrop()
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.ic_image_download_48dp)
                .into(mImageShot);

        mName.setText(player.getName());
        mTitle.setText(mShot.getTitle());
        mDescription.setText(Html.fromHtml(mShot.getDescription()));
        mDescription.setMovementMethod(LinkMovementMethod.getInstance());
        mNumberOfViews.setText(String.valueOf(mShot.getNumberOfViews()));


    }

    private void shareShot() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, mShot.getUrl());
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.shot_detail_send_to)));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_shot_detail, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_share:
                shareShot();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mOnTitleChangeListener = (OnTitleChangeListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnTitleChangeListener.");
        }
    }

}
