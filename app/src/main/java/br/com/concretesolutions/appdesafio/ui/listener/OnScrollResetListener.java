package br.com.concretesolutions.appdesafio.ui.listener;

public interface OnScrollResetListener {

    void reset();

}
