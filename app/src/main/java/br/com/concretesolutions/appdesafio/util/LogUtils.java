package br.com.concretesolutions.appdesafio.util;

import android.util.Log;

import br.com.concretesolutions.appdesafio.BuildConfig;

public class LogUtils {

    private static final String LOG_PREFIX = "DESAFIO_";

    public static void debug(final String tag, final String message) {
        debug(tag, message, null);
    }

    public static void debug(final String tag, final String message, final Throwable cause) {
        if (BuildConfig.DEBUG || Log.isLoggable(tag, Log.DEBUG)) {
            if (cause == null) {
                Log.d(buildTag(tag), message);
            } else {
                Log.d(buildTag(tag), message, cause);
            }
        }
    }

    private static String buildTag(final String tag) {
        return LOG_PREFIX + tag;
    }

}
