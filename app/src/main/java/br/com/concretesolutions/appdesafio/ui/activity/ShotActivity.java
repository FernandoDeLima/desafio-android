package br.com.concretesolutions.appdesafio.ui.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import br.com.concretesolutions.appdesafio.R;
import br.com.concretesolutions.appdesafio.model.Shot;
import br.com.concretesolutions.appdesafio.ui.fragment.ShotDetailFragment_;
import br.com.concretesolutions.appdesafio.ui.fragment.ShotListFragment_;
import br.com.concretesolutions.appdesafio.ui.listener.OnShotSelectedListener;
import br.com.concretesolutions.appdesafio.ui.listener.OnTitleChangeListener;

@EActivity(R.layout.base_layout)
public class ShotActivity extends AppCompatActivity implements OnShotSelectedListener, OnTitleChangeListener {

    @ViewById(R.id.toolbar)
    protected Toolbar mToolbar;

    @AfterViews
    protected void setupView() {
        final boolean addFragmentToBackStack = Boolean.FALSE;
        setMainFragment(new ShotListFragment_(), addFragmentToBackStack);
        initializeComponents();
        initializeListeners();
    }

    @Override
    public void onShotSelected(final Shot shot) {
        final Bundle bundle = new Bundle();
        bundle.putSerializable(ShotDetailFragment_.EXTRA_SHOT_SELECTED, shot);

        final ShotDetailFragment_ shotDetailFragment = new ShotDetailFragment_();
        shotDetailFragment.setArguments(bundle);

        final boolean addFragmentToBackStack = Boolean.TRUE;
        setMainFragment(shotDetailFragment, addFragmentToBackStack);
    }

    @Override
    public void onTitleChange(final String title) {
        setTitle(title);
    }

    private void initializeComponents() {
        setSupportActionBar(mToolbar);
    }

    private void initializeListeners() {
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
                boolean hasMoreOneFragmentInBackStack = backStackEntryCount > 0;
                if (hasMoreOneFragmentInBackStack) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                } else {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                }
            }
        });
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                setTitle(R.string.toolbar_title_shots);
            }
        });
    }

    public void setMainFragment(final Fragment fragment, final boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fragment_container, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commit();
    }
}
