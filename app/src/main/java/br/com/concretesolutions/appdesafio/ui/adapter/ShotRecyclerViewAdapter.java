package br.com.concretesolutions.appdesafio.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import br.com.concretesolutions.appdesafio.R;
import br.com.concretesolutions.appdesafio.model.Shot;

public class ShotRecyclerViewAdapter extends RecyclerView.Adapter<ShotRecyclerViewAdapter.ViewHolder>{

    public interface OnClickListener{
        void onClick(final Shot shot);
    }

    private OnClickListener onClickListener;
    private final List<Shot> mShots;

    public ShotRecyclerViewAdapter(List<Shot> shots) {
        this.mShots = shots;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.snippet_item_shot, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Shot shot = mShots.get(position);

        Glide.with(holder.mBanner.getContext())
                .load(shot.getImageURL())
                .centerCrop()
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.ic_image_download_48dp)
                .error(R.drawable.ic_image_download_48dp)
                .into(holder.mBanner);

        holder.mTitle.setText(shot.getTitle());
        holder.mNumberOfTheViews.setText(String.valueOf(shot.getNumberOfViews()));

        if(onClickListener != null){
            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.onClick(shot);
                }
            });
        }
    }

    public void setOnClickListener(final OnClickListener onClickListener){
        this.onClickListener = onClickListener;
    }

    @Override
    public int getItemCount() {
        return mShots.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final ImageView mBanner;
        public final TextView mTitle;
        public final TextView mNumberOfTheViews;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mBanner = (ImageView) view.findViewById(R.id.img_shot);
            mTitle = (TextView) view.findViewById(R.id.txv_title);
            mNumberOfTheViews = (TextView) view.findViewById(R.id.txv_number_of_views);
        }

    }

}
