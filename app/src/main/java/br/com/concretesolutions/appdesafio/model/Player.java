package br.com.concretesolutions.appdesafio.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Player implements Serializable {

    private String name;
    @SerializedName("avatar_url")
    private String avatarURL;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatarURL() {
        return avatarURL;
    }

    public void setAvatarURL(String avatarURL) {
        this.avatarURL = avatarURL;
    }
}
