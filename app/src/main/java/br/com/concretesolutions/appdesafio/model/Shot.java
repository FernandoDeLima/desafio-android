package br.com.concretesolutions.appdesafio.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import br.com.concretesolutions.appdesafio.util.StringUtils;

/**
 * Created by Fernando on 15/08/2015.
 */
public class Shot implements Serializable {

    private Long id;
    private String title;
    private String description;
    @SerializedName("views_count")
    private Integer numberOfViews;
    @SerializedName("image_url")
    private String imageURL;
    @SerializedName("image_400_url")
    private String smallerImageURL;
    private String url;
    private Player player;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        if(StringUtils.isBlank(description)){
            return StringUtils.EMPTY;
        }
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getNumberOfViews() {
        return numberOfViews;
    }

    public void setNumberOfViews(Integer numberOfViews) {
        this.numberOfViews = numberOfViews;
    }

    public String getImageURL() {
        if(StringUtils.isNotBlank(smallerImageURL)){
            return smallerImageURL;
        }
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getShortTitle(){
        if(StringUtils.isBlank(title)){
            return StringUtils.EMPTY;
        } else if(title.length() > 20){
            return title.substring(0, 20).trim() + "...";
        }
        return title;
    }

}
