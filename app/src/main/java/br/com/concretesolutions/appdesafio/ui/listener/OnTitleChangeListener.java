package br.com.concretesolutions.appdesafio.ui.listener;

/**
 * Created by Fernando on 16/08/2015.
 */
public interface OnTitleChangeListener {

    void onTitleChange(final String title);

}
