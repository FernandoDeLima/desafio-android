package br.com.concretesolutions.appdesafio.ui.listener;

import br.com.concretesolutions.appdesafio.model.Shot;

/**
 * Created by Fernando on 15/08/2015.
 */
public interface OnShotSelectedListener {

    void onShotSelected(final Shot shot);

}
